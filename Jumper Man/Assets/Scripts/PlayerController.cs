﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D _rb2D;
    private Transform _transform;

    private bool stopped = false;
    public float acceleration;

    public bool touchingPlatform;

    public Transform groundCheck;
    public LayerMask whatIsGround;
    public float groundCheckRadius;
    public bool grounded;
    
	// Use this for initialization
	void Start ()
    {
        Physics2D.IgnoreLayerCollision(11, 9, true);
        Physics2D.IgnoreLayerCollision(8, 12, true);
        _rb2D = GetComponent<Rigidbody2D>();
	    _transform = GetComponent<Transform>();
	}

    void Update()
    {
    }

    public void Jump()
    {
        if (grounded)
        {
            var force = new Vector2(0, 1f);
            _rb2D.velocity = force * acceleration;
        }
    }

    public void Boost()
    {
        throw new NotImplementedException();
    }

    public void StopMovement()
    {
        if (grounded)
        {
            stopped = true;
        }
    }

    public void ReleaseStopMovement()
    {
        stopped = false;
    }

    public void GivePoints(int amount)
    {
        ScoreManager.score += amount;
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if ((!touchingPlatform || grounded) && !stopped)
        {
            _transform.Translate(acceleration * Time.deltaTime, 0f, 0f);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        touchingPlatform = true;
        if (other.gameObject.layer == 10) // fallbounds
        {
            SceneManager.LoadScene("Main Menu2");
        } else if (other.gameObject.tag == "Moving Platform")
        {
            _transform.parent = other.transform;
        }
    }

    void OnCollisionExit2D()
    {
        touchingPlatform = false;
        _transform.parent = null;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Finish")
        {
            // TODO: Stop camera follow
            // TODO: Disable controls
            // TODO: Show finish level stats
            // TODO: Stop player movement after leaving camera bounds
            SceneManager.LoadScene("Main Menu2");
        }
        else if (collider.tag == "Finish Approaching")
        {
            // UIMessageManager.ShowMessage("")
            // Display a message object "Almost there", or "Getting close!", "Finish ahead!"
            // Message displayed for 1 second, then fades out
        }
    }
}
