﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour
{
    public Texture BackgroundTexture;
    public GUIStyle ButtonStyle;
    public float ButtonWidth;
    public float ButtonHeight;
    public float ButtonPaddingWidth;
    public float ButtonPaddingHeight;
    public float ButtonMarginWidth;


    private float GetXMenuPosition(int index)
    {
        return (ButtonMarginWidth + (ButtonPaddingWidth + ButtonWidth)*index);
    }
    void OnGUI () {

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), BackgroundTexture, ScaleMode.StretchToFill);

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(0), Screen.height * 0.05f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 1", ButtonStyle))
        {
            SceneManager.LoadScene("Level1");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(1), Screen.height * 0.05f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 2", ButtonStyle))
        {
            SceneManager.LoadScene("Level2");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(2), Screen.height * 0.05f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 3", ButtonStyle))
        {
            SceneManager.LoadScene("Level3");
            // TODO: Unload previous scene?
        }
        
        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(3), Screen.height * 0.05f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 4", ButtonStyle))
        {
            SceneManager.LoadScene("Level4");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(4), Screen.height * 0.05f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 5", ButtonStyle))
        {
            SceneManager.LoadScene("Level5");
            // TODO: Unload previous scene?
        }


        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(0), Screen.height * 0.35f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 6", ButtonStyle))
        {
            SceneManager.LoadScene("Level6");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(1), Screen.height * 0.35f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 7", ButtonStyle))
        {
            SceneManager.LoadScene("Level7");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(2), Screen.height * 0.35f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 8", ButtonStyle))
        {
            SceneManager.LoadScene("Level8");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(3), Screen.height * 0.35f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 9", ButtonStyle))
        {
            SceneManager.LoadScene("Level9");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * GetXMenuPosition(4), Screen.height * 0.35f, Screen.width * ButtonWidth, Screen.height * ButtonHeight), "Level 10", ButtonStyle))
        {
            SceneManager.LoadScene("Level10");
            // TODO: Unload previous scene?
        }

        if (GUI.Button(new Rect(Screen.width * 0.05f, Screen.height * 0.8f, Screen.width * 0.25f, Screen.height * 0.1f), "Back", ButtonStyle))
        {
            SceneManager.LoadScene("Main Menu2");
            // TODO: Unload previous scene?
        }

    }
}
