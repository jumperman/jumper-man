﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadOnClick : MonoBehaviour
{
    public List<Sprite> spriteList;

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadImage(string movement)
    {
        var imageScript = GameObject.Find("imgHowToScreen");
        var image = imageScript.GetComponent<Image>();
        var spritePos = spriteList.IndexOf(image.sprite);

        if (movement == "Next")
        {
            spritePos++;
        }
        else
        {
            spritePos--;
        }

        if (spritePos > 3)
        {
            spritePos = 0;
        }
        else if (spritePos < 0)
        {
            spritePos = 3;
        }
        
        image.sprite = spriteList[spritePos];
    }

    public void Quit()
    {
        Application.Quit();
    }
}
