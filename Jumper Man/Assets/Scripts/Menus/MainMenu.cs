﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Texture BackgroundTexture;
    public GUIStyle ButtonStyle;
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), BackgroundTexture, ScaleMode.StretchToFill);
        if (GUI.Button(new Rect(Screen.width*0.25f, Screen.height*0.5f, Screen.width*0.5f, Screen.height*0.1f), "New Game", ButtonStyle))
        {
            SceneManager.LoadScene("Level Selection 2");
            // TODO: Unload previous scene?
        }
        if (GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.625f, Screen.width * 0.5f, Screen.height * 0.1f), "How To Play", ButtonStyle))
        {
            SceneManager.LoadScene("How To Play");
            // TODO: Unload previous scene?
        }
        if (GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.75f, Screen.width * 0.5f, Screen.height * 0.1f), "High Scores", ButtonStyle))
        {
            SceneManager.LoadScene("High Scores");
            // TODO: Unload previous scene?
        }
        if (GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.875f, Screen.width * 0.5f, Screen.height * 0.1f), "Quit", ButtonStyle))
        {
            Application.Quit();
            // On "New Game" click
        }
    }
}
