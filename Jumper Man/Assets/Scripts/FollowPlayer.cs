﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
    public GameObject followObject;
    private Transform _transform;
    private Camera _camera;
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start ()
	{
	    _camera = GetComponent<Camera>();
        Physics2D.IgnoreLayerCollision(8, 11, true);
        Physics2D.IgnoreLayerCollision(9, 11, true);
        Physics2D.IgnoreLayerCollision(10, 11, false);
        Physics2D.IgnoreLayerCollision(8, 12, true);
        Physics2D.IgnoreLayerCollision(9, 12, true);
        Physics2D.IgnoreLayerCollision(10, 12, false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 point = _camera.WorldToViewportPoint(followObject.transform.position);
        Vector3 delta = followObject.transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        //    _transform = GetComponent<Transform>();
        //    var followTransform = followObject.GetComponent<Transform>();
        // var player = new Vector3(followTransform.position.x, followTransform.position.y, -10f);
        //    _transform.position = Vector3.SmoothDamp(_transform.position, player);
    }
    }
