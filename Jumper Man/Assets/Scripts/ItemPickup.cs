﻿using UnityEngine;
using System.Collections;

public class ItemPickup : MonoBehaviour {

    public PlayerController Player;
    public int PointValue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 8)
        {
            Player.GivePoints(PointValue);
            // TODO: Give player points
            Destroy(gameObject);
        }
    }
}
