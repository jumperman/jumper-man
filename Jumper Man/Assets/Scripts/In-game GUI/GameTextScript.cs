﻿using UnityEngine;
using System.Collections;

public class GameTextScript : MonoBehaviour {

    private const float WaitTime = 1f;
    private SpriteRenderer _sprite;
    private bool displayMessage = false;
    private float startTime;
    // Use this for initialization
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_sprite.enabled && startTime <= 0)
        {
            displayMessage = true;
            startTime = Time.time;
        }

        if (displayMessage && (Time.time - startTime) >= WaitTime)
        {
            displayMessage = false;
            _sprite.enabled = false;
            startTime = 0;
        }
    }
}
